import logging

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.template import loader
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

import common.help_texts
from extensions.models import Extension
from common.model_mixins import CreatedModifiedMixin, RecordDeletionMixin
from utils import absolutify, send_mail

from constants.base import EXTENSION_TYPE_CHOICES
from constants.reviewers import CANNED_RESPONSE_CATEGORY_CHOICES

User = get_user_model()
logger = logging.getLogger('users')


class CannedResponse(CreatedModifiedMixin, models.Model):
    TYPES = EXTENSION_TYPE_CHOICES
    CATEGORIES = CANNED_RESPONSE_CATEGORY_CHOICES

    name = models.CharField(max_length=255)
    response = models.TextField()
    sort_group = models.CharField(max_length=255)
    type = models.PositiveIntegerField(choices=TYPES, db_index=True, default=TYPES.BPY)

    # Category is used only by code-manager
    category = models.PositiveIntegerField(choices=CATEGORIES, default=CATEGORIES.OTHER)

    def __str__(self):
        return str(self.name)


class ReviewerSubscription(CreatedModifiedMixin, models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    extension = models.ForeignKey(Extension, on_delete=models.CASCADE)

    def send_notification(self, version):
        logger.info(
            'Sending extension update notice to %s for %s' % (self.user.email, self.extension.pk)
        )

        listing_url = absolutify(
            reverse('extensions.detail', args=[self.extension.pk], add_prefix=False)
        )
        context = {
            'name': self.extension.name,
            'url': listing_url,
            'number': version.version,
            'review': absolutify(
                reverse(
                    'reviewers.review',
                    kwargs={
                        'extension_id': self.extension.pk,
                    },
                    add_prefix=False,
                )
            ),
            'SITE_URL': settings.SITE_URL,
        }
        # Not being localised because we don't know the reviewer's locale.
        subject = 'Blender Extensions: %s Updated' % self.extension.name
        template = loader.get_template('reviewers/emails/notify_update.ltxt')
        send_mail(
            subject,
            template.render(context),
            recipient_list=[self.user.email],
            from_email=settings.EXTENSIONS_EMAIL,
            use_deny_list=False,
        )


class ApprovalActivity(CreatedModifiedMixin, RecordDeletionMixin, models.Model):
    class ActivityType(models.TextChoices):
        COMMENT = "COM", _("Comment")
        APPROVED = "APR", _("Approved")
        AWAITING_CHANGES = "AWC", _("Awaiting Changes")
        AWAITING_REVIEW = "AWR", _("Awaiting Review")
        UPLOADED_NEW_VERSION = "UNV", _("Uploaded New Version")

    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    extension = models.ForeignKey(
        Extension,
        on_delete=models.CASCADE,
        related_name='review_activity',
    )
    type = models.CharField(
        max_length=3,
        choices=ActivityType.choices,
        default=ActivityType.COMMENT,
    )
    message = models.TextField(help_text=common.help_texts.markdown, blank=False, null=False)

    class Meta:
        verbose_name_plural = "Review activity"

    def __str__(self):
        return f"{self.extension.name}: {self.get_type_display()}"
