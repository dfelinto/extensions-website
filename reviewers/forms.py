from django import forms
import reviewers.models


class CommentForm(forms.ModelForm):
    class Meta:
        model = reviewers.models.ApprovalActivity
        fields = ('message', 'type')
        widgets = {
            'message': forms.Textarea(attrs={'placeholder': 'Add your comment here...'}),
        }
