from django.test import TestCase
from django.shortcuts import reverse

from common.tests.factories.extensions import create_version
from files.models import File
from reviewers.models import ApprovalActivity


class CommentsViewTest(TestCase):
    fixtures = ['licenses']

    def setUp(self):
        version = create_version(file__status=File.STATUSES.AWAITING_REVIEW)
        self.default_version = version
        ApprovalActivity(
            type=ApprovalActivity.ActivityType.COMMENT,
            user=version.file.user,
            extension=version.extension,
            message='test comment',
        ).save()

    # List of extensions under review does not require authentication
    def test_list_visibility(self):
        r = self.client.get(reverse('reviewers:approval-queue'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(r.context['object_list']), 1)

    # Page is visible for every extension and does not require authentication
    def test_visibility(self):
        r = self.client.get(
            reverse(
                'reviewers:approval-detail',
                kwargs={'slug': self.default_version.extension.slug},
            )
        )
        self.assertEqual(r.status_code, 200)

    # Activity form display requires authentication

    # Display only previews and version that are 'pending review'

    # Approval requires manager role
