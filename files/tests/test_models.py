import json

from django.contrib.auth import get_user_model
from django.test import TestCase

from common.admin import get_admin_change_path
from common.log_entries import entries_for
from common.tests.factories.files import FileFactory
from files.models import File

User = get_user_model()


class FileTest(TestCase):
    maxDiff = None
    fixtures = ['dev', 'licenses']

    def setUp(self):
        super().setUp()
        self.file = FileFactory(
            status=File.STATUSES.AWAITING_REVIEW,
            original_name='test.zip',
            hash='foobar',
            size_bytes=7149,
        )
        self.assertEqual(entries_for(self.file).count(), 0)
        self.assertIsNone(self.file.date_approved)
        self.assertIsNone(self.file.date_status_changed)

    def _check_change_message(self):
        entries = entries_for(self.file)
        self.assertEqual(entries.count(), 1)
        log_entry = entries.first()
        change_message = json.loads(log_entry.change_message)
        self.assertEqual(len(change_message), 1)
        self.assertDictEqual(
            change_message[0],
            {
                'changed': {
                    'fields': ['status'],
                    'name': 'file',
                    'new_state': {'status': 'Approved'},
                    'object': '<File: test.zip (Approved)>',
                    'old_state': {
                        'hash': 'foobar',
                        'metadata': {},
                        'size_bytes': 7149,
                        'status': 2,
                        'thumbnail': '',
                    },
                }
            },
        )

    def test_status_change_updates_date_creates_log_entry(self):
        self.file.status = File.STATUSES.APPROVED
        self.file.save()

        self.assertIsNotNone(self.file.date_approved)
        self.assertIsNotNone(self.file.date_status_changed)
        self._check_change_message()

    def test_status_change_updates_date_creates_log_entry_with_update_fields(self):
        self.file.status = File.STATUSES.APPROVED
        self.file.save(update_fields={'status'})

        self.assertIsNotNone(self.file.date_approved)
        self.assertIsNotNone(self.file.date_status_changed)
        self._check_change_message()

    def test_admin_change_view(self):
        path = get_admin_change_path(obj=self.file)
        self.assertEqual(path, '/admin/files/file/1/change/')

        admin_user = User.objects.get(pk=1)
        self.client.force_login(admin_user)
        response = self.client.get(path)

        self.assertEqual(response.status_code, 200, path)
