import os
import os.path
import shutil
import tempfile
import unittest

from background_task.models import Task
from django.conf import settings
from django.test import TestCase, override_settings

from common.tests.factories.files import FileFactory
import files.models
import files.tasks


@unittest.skipUnless(os.path.exists('/var/run/clamav/clamd.ctl'), 'requires clamd running')
@override_settings(MEDIA_ROOT='/tmp/')
class FileScanTest(TestCase):
    def setUp(self):
        super().setUp()
        self.temp_directory = tempfile.mkdtemp(prefix=settings.MEDIA_ROOT)

    def tearDown(self):
        super().tearDown()
        shutil.rmtree(self.temp_directory)

    def test_scan_flags_found_invalid(self):
        test_file_path = os.path.join(self.temp_directory, 'test_file.zip')
        test_content = (
            b'X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'  # noqa: W605
        )
        with open(test_file_path, 'wb+') as test_file:
            test_file.write(test_content)

        file = FileFactory(source=test_file_path)
        self.assertFalse(hasattr(file, 'validation'))

        # A background task should have been created
        task = Task.objects.created_by(creator=file).first()
        self.assertIsNotNone(task)
        self.assertEqual(task.task_name, 'files.tasks.clamdscan')
        self.assertEqual(task.task_params, f'[[], {{"file_id": {file.pk}}}]')

        # Actually run the task as if by background runner
        task_args, task_kwargs = task.params()
        files.tasks.clamdscan.task_function(*task_args, **task_kwargs)

        file.refresh_from_db()
        self.assertFalse(file.validation.is_ok)
        result = file.validation.results['clamdscan']
        self.assertEqual(result, ['FOUND', 'Win.Test.EICAR_HDB-1'])

    def test_scan_flags_found_invalid_updates_existing_validation(self):
        test_file_path = os.path.join(self.temp_directory, 'test_file.zip')
        test_content = (
            b'X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'  # noqa: W605
        )
        with open(test_file_path, 'wb+') as test_file:
            test_file.write(test_content)

        file = FileFactory(source=test_file_path)
        # Make sure validation record exists before scanner runs
        existing_validation = files.models.FileValidation(file=file, results={})
        existing_validation.save()
        self.assertTrue(hasattr(file, 'validation'))
        old_date_modified = existing_validation.date_modified

        # A background task should have been created
        task = Task.objects.created_by(creator=file).first()
        self.assertIsNotNone(task)
        self.assertEqual(task.task_name, 'files.tasks.clamdscan')
        self.assertEqual(task.task_params, f'[[], {{"file_id": {file.pk}}}]')

        # Actually run the task as if by background runner
        task_args, task_kwargs = task.params()
        files.tasks.clamdscan.task_function(*task_args, **task_kwargs)

        self.assertFalse(file.validation.is_ok)
        file.validation.refresh_from_db()
        result = file.validation.results['clamdscan']
        self.assertEqual(result, ['FOUND', 'Win.Test.EICAR_HDB-1'])
        self.assertEqual(existing_validation.pk, file.validation.pk)

        existing_validation.refresh_from_db()
        self.assertGreater(existing_validation.date_modified, old_date_modified)

    def test_scan_flags_nothing_found_valid(self):
        test_file_path = os.path.join(self.temp_directory, 'test_file.zip')
        with open(test_file_path, 'wb+') as test_file:
            test_file.write(b'some file')

        file = FileFactory(source=test_file_path)
        self.assertFalse(hasattr(file, 'validation'))

        # A background task should have been created
        task = Task.objects.created_by(creator=file).first()
        self.assertIsNotNone(task)
        self.assertEqual(task.task_name, 'files.tasks.clamdscan')
        self.assertEqual(task.task_params, f'[[], {{"file_id": {file.pk}}}]')

        # Actually run the task as if by background runner
        task_args, task_kwargs = task.params()
        files.tasks.clamdscan.task_function(*task_args, **task_kwargs)

        file.refresh_from_db()
        self.assertTrue(file.validation.is_ok)
        result = file.validation.results['clamdscan']
        self.assertEqual(result, ['OK', None])
