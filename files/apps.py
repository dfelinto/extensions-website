from django.apps import AppConfig


class FilesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'files'

    def ready(self):
        import files.signals  # noqa: F401

        # Ubuntu 22.04 and earlier don't have WebP in `/etc/mime.types`,
        # which makes .webp invalid from standpoint of file upload forms.
        # FIXME: remove once the application is running on the next Ubuntu 24.04 LTS
        import mimetypes

        mimetypes.add_type('image/webp', '.webp', strict=True)
