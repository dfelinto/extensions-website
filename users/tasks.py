"""Background tasks for user-related things."""
from datetime import timedelta
import logging

from background_task import background
from django.contrib.auth import get_user_model
from django.utils import timezone

from users.blender_id import BIDSession

User = get_user_model()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
DELETION_DELTA = timedelta(weeks=2)

bid = BIDSession()


@background()
def handle_deletion_request(pk: int) -> bool:
    """Delete user account and all data related to it."""
    prior_to = timezone.now() - DELETION_DELTA
    user = User.objects.get(
        date_deletion_requested__isnull=False,
        date_deletion_requested__lt=prior_to,
        pk=pk,
    )

    if not user.can_be_deleted:
        logger.error('Cannot delete user pk=%s', pk)
        return False

    user.anonymize_or_delete()
    return True


@background()
def grant_blender_id_role(pk: int, role: str, **kwargs) -> bool:
    """Call Blender ID API to grant a given role to a user with given ID."""
    user = User.objects.get(pk=pk)
    bid.grant_revoke_role(user, action='grant', role=role)
    bid.copy_badges_from_blender_id(user=user)
    return True


@background()
def revoke_blender_id_role(pk: int, role: str, **kwargs) -> bool:
    """Call Blender ID API to revoke given roles from a user with given ID."""
    user = User.objects.get(pk=pk)
    bid.grant_revoke_role(user, action='revoke', role=role)
    bid.copy_badges_from_blender_id(user=user)
    return True
