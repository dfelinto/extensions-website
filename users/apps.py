from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = 'Authentication and authorization'

    def ready(self) -> None:
        from actstream import registry
        from django.contrib.auth import get_user_model
        import users.signals  # noqa: F401

        registry.register(get_user_model())
