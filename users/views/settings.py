"""User profile pages."""
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.edit import FormView

from users.forms import SubscribeNotificationEmailsForm

User = get_user_model()


class ProfileView(LoginRequiredMixin, TemplateView):
    """Template view for the profile settings."""

    template_name = 'users/settings/profile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subscribe_notification_emails_form'] = SubscribeNotificationEmailsForm(
            {'subscribe': not self.request.user.is_subscribed_to_notification_emails},
        )
        return context


class DeleteView(LoginRequiredMixin, TemplateView):
    """Template view where account deletion can be requested."""

    template_name = 'users/settings/delete.html'


class SubscribeNotificationEmailsView(LoginRequiredMixin, FormView):
    form_class = SubscribeNotificationEmailsForm
    success_url = reverse_lazy('users:my-profile')

    def form_valid(self, form):
        self.request.user.is_subscribed_to_notification_emails = form.cleaned_data['subscribe']
        self.request.user.save(update_fields={'is_subscribed_to_notification_emails'})
        return super().form_valid(form)
