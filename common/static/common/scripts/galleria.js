function galleriaCloneFirstItem() {
  const galleriaCarrousel = document.getElementById('galleria-items');
  if (!galleriaCarrousel) { return;}

  let firstGalleriaItem = galleriaCarrousel.firstElementChild.cloneNode(true);
  firstGalleriaItem.classList.remove('js-galleria-item-preview', 'is-active');
  firstGalleriaItem.classList.add('js-expand-on-click');
  firstGalleriaItem.id = 'galleria-item-lg';

  document.getElementById("galleria-container").prepend(firstGalleriaItem);
}


function galleriaSetLargePreview(item) {
  let previewsContainer = document.getElementById('galleria-items');
  let previewLarge = document.getElementById('galleria-item-lg');
  let thumbnails = document.getElementsByClassName("js-galleria-item-preview");
  [].forEach.call(thumbnails, function(el) {
    el.classList.remove("is-active");
  });

  const galleryItem = item.firstElementChild;
  const galleriaIndex = item.dataset.galleriaIndex;
  const galleriaContentType = item.dataset.galleriaContentType;
  const galleriaVideoUrl = item.dataset.galleriaVideoUrl;

  previewLarge.classList = item.classList;
  previewLarge.firstElementChild.src = galleryItem.src;
  previewLarge.firstElementChild.alt = galleryItem.alt;
  previewLarge.dataset.galleriaIndex = galleriaIndex;
  previewLarge.dataset.galleriaContentType = galleriaContentType;
  previewLarge.dataset.galleriaVideoUrl = galleriaVideoUrl;

  /* Scroll the container as we click on items. */
  previewsContainer.scrollLeft = item.offsetLeft;
}


function galleriaCreateCaption(captionText, overlay) {
  let captionContainer = document.getElementById('galleria-caption');

  if (captionContainer === null) {
    captionContainer = document.createElement("div");
    captionContainer.id = "galleria-caption";
  }

  if (captionText == "") {
    captionContainer.remove();
    return;
  }

  captionContainer.innerHTML = captionText;
  captionContainer.classList.add("caption");
  overlay.appendChild(captionContainer);
}


/* Scrollable section with all media thumbnails. */
function galleriaScrollNavigation() {
  let thumbnails = document.getElementsByClassName("js-galleria-item-preview");

  // Attach click event listener to each item.
  Array.from(thumbnails).forEach(function(item) {
    item.addEventListener('click', function(e) {
      e.preventDefault();

      galleriaSetLargePreview(item);
      item.classList.add('is-active');
    });
  });
}


function galleriaCreateOverlay() {
  let overlay = document.createElement("div");
  overlay.classList.add("galleria");
  document.body.classList.add('is-galleria-active');

  return overlay;
}


function galleriaCloseOverlay(overlay) {
  if (overlay.parentNode === document.body) {
    document.body.removeChild(overlay);
    document.body.classList.remove('is-galleria-active');
  }
}


function galleriaCreateUnderlay() {
  let underlay = document.createElement("div");
  underlay.classList.add("underlay");

  return underlay;
}


function galleriaCreateLoadingPlaceholder() {
  let loadingPlaceholder = document.createElement("span");
  loadingPlaceholder.innerHTML = "...";
  return loadingPlaceholder;
}


/* Create Image element. */
function galleriaCreateMediaImage(galleriaItem, overlay, loadingPlaceholder) {
  let galleriaNewItem = new Image();
  galleriaNewItem.id = 'galleria-active-item';

  galleriaNewItem.onload = function () {
    if (overlay.contains(loadingPlaceholder)) {
      overlay.replaceChild(galleriaNewItem, loadingPlaceholder);
    }
  };

  galleriaNewItem.src = galleriaItem.firstElementChild.src;
  galleriaNewItem.alt = galleriaItem.firstElementChild.alt;

  galleriaCreateCaption(galleriaNewItem.alt, overlay);
}


/* Create Video element. */
function galleriaCreateMediaVideo(galleriaItem, overlay, loadingPlaceholder) {
  let galleriaNewItem = document.createElement('video');
  galleriaNewItem.id = 'galleria-active-item';

  galleriaNewItem.onloadeddata = function () {
    if (overlay.contains(loadingPlaceholder)) {
      overlay.replaceChild(galleriaNewItem, loadingPlaceholder);
    }
  };

  galleriaNewItem.src = galleriaItem.dataset.galleriaVideoUrl;
  galleriaNewItem.poster = galleriaItem.firstElementChild.src;
  galleriaNewItem.muted = true;
  galleriaNewItem.controls = true;
  galleriaNewItem.loop = true;
  galleriaNewItem.autoplay = true;

  galleriaCreateCaption(galleriaItem.firstElementChild.alt, overlay);
}


function galleriaCreateMedia(galleriaItem, galleriaContentType, overlay) {
  const activeItem = overlay.querySelector('#galleria-active-item');
  const loadingPlaceholder = galleriaCreateLoadingPlaceholder();

  if (activeItem) {
    activeItem.remove();
  }

  if (galleriaContentType.includes('video')) {
    galleriaCreateMediaVideo(galleriaItem, overlay, loadingPlaceholder);
  } else {
    galleriaCreateMediaImage(galleriaItem, overlay, loadingPlaceholder);
  }


  overlay.appendChild(loadingPlaceholder);
}


function galleriaStatsUpdateIndex(currentIndex, total) {
  let statsContainer = document.getElementById('galleria-current-index');
  statsContainer.innerHTML = currentIndex + "/" + total;
}


function galleriaNavigatePrev(siblings, currentIndex, overlay) {
  let prevGalleriaItem;
  let prevIndex = parseInt(currentIndex) - 1;
  currentIndex = (prevIndex % siblings.length);

  /* If there are no more items, go back to the last. */
  if (prevIndex < 1) {
    prevIndex = siblings.length;
    prevGalleriaItem = siblings[prevIndex - 1].getElementsByTagName("img")[0];
  } else {
    prevGalleriaItem = siblings[currentIndex - 1].getElementsByTagName("img")[0];
  }

  const mediaElement = siblings[prevIndex - 1];
  const mediaContentType = mediaElement.dataset.galleriaContentType;
  galleriaCreateMedia(mediaElement, mediaContentType, overlay);

  galleriaStatsUpdateIndex(prevIndex, siblings.length);

  return prevIndex;
}


function galleriaNavigateNext(siblings, currentIndex, overlay) {
  /* If there are no more items, go back to the first. */
  if (!siblings[currentIndex]) {
    currentIndex = 0;
  }

  const mediaElement = siblings[currentIndex];
  const mediaContentType = mediaElement.dataset.galleriaContentType;
  galleriaCreateMedia(mediaElement, mediaContentType, overlay);

  currentIndex = (parseInt(currentIndex) % siblings.length) + 1;
  galleriaStatsUpdateIndex(currentIndex, siblings.length);

  return currentIndex;
}


function galleriaCreateNavigationDiv(siblings, currentIndex, overlay) {
  let navigationDiv = document.createElement("div");

  let prevButton = document.createElement("button");
  prevButton.innerHTML = '<i class="i-chevron-left"></i>'
  prevButton.classList.add("btn", "btn-prev");

  let nextButton = document.createElement("button");
  nextButton.innerHTML = '<i class="i-chevron-right"></i>'
  nextButton.classList.add("btn", "btn-next");

  let closeButton = document.createElement("button");
  closeButton.innerHTML = '<i class="i-cancel"></i>'
  closeButton.classList.add("btn", "btn-close");
  navigationDiv.appendChild(closeButton);

  closeButton.addEventListener("click", function () {
    galleriaCloseOverlay(overlay);
  });

  if (siblings.length > 1) {
    navigationDiv.appendChild(prevButton);

    prevButton.addEventListener("click", function () {
      currentIndex = galleriaNavigatePrev(siblings, currentIndex, overlay);
    });

    navigationDiv.appendChild(nextButton);

    nextButton.addEventListener("click", function () {
      currentIndex = galleriaNavigateNext(siblings, currentIndex, overlay);
    });
  }

  return navigationDiv;
}


function galleriaPageIndicator() {
  let container = document.createElement("div");
  let currentSpan = document.createElement("span");
  currentSpan.id = 'galleria-current-index';
  container.appendChild(currentSpan);
  container.classList.add("indicator");

  return container;
}


function galleriaCreate() {
  let galleriaItems = Array.from(document.body.getElementsByClassName("js-expand-on-click"));

  // Attach click event listener to each galleria item.
  Array.from(galleriaItems).forEach(function(galleriaItem) {
    galleriaItem.addEventListener('click', function(e) {
      e.preventDefault();

      let entryContent = document.getElementsByClassName("galleria-items")[0];
      let siblings = Array.from(entryContent.getElementsByClassName("js-galleria-item-preview"));
      let currentIndex = galleriaItem.dataset.galleriaIndex;
      const currentContentType = galleriaItem.dataset.galleriaContentType;

      let overlay = galleriaCreateOverlay();
      let navigationDiv = galleriaCreateNavigationDiv(siblings, currentIndex, overlay);
      overlay.appendChild(navigationDiv);

      let stats = galleriaPageIndicator();
      overlay.appendChild(stats);

      document.body.appendChild(overlay);

      galleriaCreateMedia(galleriaItem, currentContentType, overlay);

      let underlay = galleriaCreateUnderlay();
      overlay.appendChild(underlay);
      underlay.addEventListener("click", function () {
        galleriaCloseOverlay(overlay);
      });

      galleriaStatsUpdateIndex(currentIndex, siblings.length);

      // Set focus to the overlay, for keyboard events.
      overlay.focus();

      // Keyboard event listeners
      document.addEventListener("keydown", function (event) {
        if (overlay && event.key === "Escape") {
          galleriaCloseOverlay(overlay);
        } else if (overlay && event.key === "ArrowRight") {
          currentIndex = galleriaNavigateNext(siblings, currentIndex, overlay);
        } else if (overlay && event.key === "ArrowLeft") {
          currentIndex = galleriaNavigatePrev(siblings, currentIndex, overlay);
        }
      });
    });
  });
}


function galleriaSetImageAttributes(imageSource, galleriaItemElement) {
  galleriaItemElement.src = imageSource.src;
  galleriaItemElement.alt = imageSource.alt;
}


function initGalleria() {
  const galleriaContainer = document.getElementById('galleria-container');
  if (!galleriaContainer) { return;}

  galleriaCloneFirstItem();
  galleriaScrollNavigation();
  galleriaCreate();
}
