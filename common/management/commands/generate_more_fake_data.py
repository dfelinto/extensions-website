import logging
import random

from django.core.management.base import BaseCommand
from django.db import transaction
from django.db.models import signals
import factory

from common.tests.factories.extensions import create_version, RatingFactory
from common.tests.factories.files import FileFactory
from files.models import File
from constants.licenses import LICENSE_GPL2, LICENSE_GPL3
from extensions.models import Extension, Tag
from utils import slugify, chunked
import faker

_faker = faker.Faker()

FILE_SOURCES = {
    "blender-kitsu": {
        "file": 'files/ed/ed656b177b01999e6fcd0e37c34ced471ef88c89db578f337e40958553dca5d2.zip',
        "hash": "sha256:3d2972a6f6482e3c502273434ca53eec0c5ab3dae628b55c101c95a4bc4e15b2",
        "size": 856650,
    },
}
PREVIEW_SOURCES = (
    'images/b0/b03fa981527593fbe15b28cf37c020220c3d83021999eab036b87f3bca9c9168.png',
    'images/be/bee9e018e80aee1176019a7ef979a3305381b362d5dedc99b9329a55bfa921b9.png',
    'images/cf/cfe6569d16ed50d3beffc13ccb3c4375fe3b0fd734197daae3c80bc6d496430f.png',
    'images/f4/f4c377d3518b6e17865244168c44b29f61c702b01a34aeb3559f492b1b744e50.png',
    'images/dc/dc8d57c6af69305b2005c4c7c71eff3db855593942bebf3c311b4d3515e955f0.png',
    'images/09/091b7bc282e8137a79c46f23d5dea4a97ece3bc2f9f0ca9c3ff013727c22736b.png',
)
EXAMPLE_DESCRIPTION = '''# blender-kitsu
blender-kitsu is a Blender Add-on to interact with Kitsu from within Blender.
It also has features that are not directly related to Kitsu but support certain
aspects of the Blender Studio Pipeline.

## Table of Contents
- [Installation](#installation)
- [How to get started](#how-to-get-started)
- [Features](#features)
    - [Sequence Editor](#sequence-editor)
    - [Context](#context)
- [Troubleshoot](#troubleshoot)

## Installation
Download or clone this repository.
In the root project folder you will find the 'blender_kitsu' folder.
Place this folder in your Blender addons directory or create a sym link to it.

## How to get started
After installing you need to setup the addon preferences to fit your environment.
In order to be able to log in to Kitsu you need a server that runs
the Kitsu production management suite.
Information on how to set up Kitsu can be found [here](https://zou.cg-wire.com/).

If Kitsu is up and running and you can succesfully log in via the web interface you have
to setup the `addon preferences`.

...
'''
LICENSES = (LICENSE_GPL2.id, LICENSE_GPL3.id)


class Command(BaseCommand):
    help = 'Generate fake data with extensions, users and versions using test factories.'

    def add_arguments(self, parser):
        parser.add_argument('--approved-add-ons', type=int, default=0, dest='approved_add_ons')
        parser.add_argument('--approved-themes', type=int, default=0, dest='approved_themes')

        parser.add_argument('--disabled-add-ons', type=int, default=0, dest='disabled_add_ons')
        parser.add_argument('--disabled-themes', type=int, default=0, dest='disabled_themes')

        parser.add_argument('--awaiting-add-ons', type=int, default=0, dest='awaiting_add_ons')
        parser.add_argument('--awaiting-themes', type=int, default=0, dest='awaiting_themes')

    def handle(self, *args, **options):
        verbosity = int(options['verbosity'])
        root_logger = logging.getLogger('root')
        if verbosity > 2:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger.setLevel(logging.WARNING)

        tags = {
            type_id: list(Tag.objects.filter(type=type_id).values_list('name', flat=True))
            for type_id, _ in Extension.TYPES
        }

        # Create a few publicly listed extensions
        e_sts = Extension.STATUSES
        f_sts = File.STATUSES
        e_t = Extension.TYPES
        for number, _type, file_status, extension_status in (
            (options['approved_add_ons'], e_t.BPY, f_sts.APPROVED, e_sts.APPROVED),
            (options['approved_themes'], e_t.THEME, f_sts.APPROVED, e_sts.APPROVED),
            (options['awaiting_add_ons'], e_t.BPY, f_sts.AWAITING_REVIEW, e_sts.AWAITING_REVIEW),
            (options['awaiting_themes'], e_t.THEME, f_sts.AWAITING_REVIEW, e_sts.AWAITING_REVIEW),
            (options['disabled_add_ons'], e_t.BPY, f_sts.AWAITING_REVIEW, e_sts.DISABLED),
            (options['disabled_themes'], e_t.THEME, f_sts.AWAITING_REVIEW, e_sts.DISABLED),
        ):
            for i_chunked in chunked(range(number), 100):
                with factory.django.mute_signals(signals.post_save, signals.pre_save):
                    with transaction.atomic():
                        for _ in i_chunked:
                            name = _faker.catch_phrase() + _faker.bothify('###???')
                            extension_id = name.replace(' ', '_')
                            slug = slugify(extension_id)[:50]
                            version = create_version(
                                file__status=file_status,
                                file__metadata={'name': name, 'id': extension_id},
                                tags=random.sample(tags[_type], k=1),
                                extension__extension_id=extension_id,
                                extension__is_listed=extension_status == e_sts.APPROVED,
                                extension__name=name,
                                extension__slug=slug,
                                extension__status=extension_status,
                                extension__type=_type,
                                extension__previews=[
                                    FileFactory(
                                        type=File.TYPES.IMAGE,
                                        source=source,
                                        status=file_status,
                                    )
                                    for source in random.sample(
                                        PREVIEW_SOURCES,
                                        k=random.randint(1, len(PREVIEW_SOURCES) - 1),
                                    )
                                ],
                                # Create these separately
                                ratings=[],
                            )
                            for i in range(random.randint(1, len(LICENSES))):
                                version.licenses.add(LICENSES[i])
                            if version.is_listed:
                                for __ in range(random.randint(1, 10)):
                                    RatingFactory(version=version, extension=version.extension)
