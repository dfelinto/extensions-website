from django.contrib.contenttypes.models import ContentType
from factory.django import DjangoModelFactory
from faker import Faker
import actstream.models
import factory

from common.tests.factories.extensions import ExtensionFactory, RatingFactory
from common.tests.factories.reviewers import ApprovalActivityFactory
from common.tests.factories.users import UserFactory
from constants.activity import Verb
import notifications.models
import reviewers.models


RELATION_ALLOWED_MODELS = []
fake = Faker()


def generic_foreign_key_id_for_type_factory(generic_relation_type_field):
    def generic_foreign_key_id(obj):
        model = getattr(obj, generic_relation_type_field).model_class()
        related = ContentType.objects.get_for_model(model)
        return related.id

    return factory.LazyAttribute(generic_foreign_key_id)


class ContentTypeFactory(DjangoModelFactory):
    class Meta:
        model = ContentType


class ActionFactory(DjangoModelFactory):
    class Meta:
        model = actstream.models.Action


class NotificationFactory(DjangoModelFactory):
    class Meta:
        model = notifications.models.Notification

    action = factory.SubFactory(ActionFactory)


def construct_fake_notifications() -> list['NotificationFactory']:
    """Construct notifications of known types without persisting them in the DB."""
    fake_extension = ExtensionFactory.build(slug='test')
    verb_to_action_object = {
        Verb.APPROVED: ApprovalActivityFactory.build(
            extension=fake_extension,
            type=reviewers.models.ApprovalActivity.ActivityType.APPROVED,
            message=fake.paragraph(nb_sentences=1),
        ),
        Verb.COMMENTED: ApprovalActivityFactory.build(
            extension=fake_extension,
            type=reviewers.models.ApprovalActivity.ActivityType.COMMENT,
            message=fake.paragraph(nb_sentences=1),
        ),
        Verb.RATED_EXTENSION: RatingFactory.build(
            text=fake.paragraph(nb_sentences=2),
        ),
        Verb.REPORTED_EXTENSION: None,  # TODO: fake action_object
        Verb.REPORTED_RATING: None,  # TODO: fake action_object
        Verb.REQUESTED_CHANGES: ApprovalActivityFactory.build(
            extension=fake_extension,
            type=reviewers.models.ApprovalActivity.ActivityType.AWAITING_CHANGES,
            message=fake.paragraph(nb_sentences=1),
        ),
        Verb.REQUESTED_REVIEW: ApprovalActivityFactory.build(
            extension=fake_extension,
            type=reviewers.models.ApprovalActivity.ActivityType.AWAITING_REVIEW,
            message=fake.paragraph(nb_sentences=1),
        ),
    }
    fake_notifications = [
        NotificationFactory.build(
            recipient=UserFactory.build(),
            action__actor=UserFactory.build(),
            action__target=fake_extension,
            action__verb=verb,
            action__action_object=action_object,
        )
        for verb, action_object in verb_to_action_object.items()
    ]
    return fake_notifications
