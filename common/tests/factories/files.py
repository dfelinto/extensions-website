from factory.django import DjangoModelFactory
import factory
import factory.fuzzy

from files.models import File


class FileFactory(DjangoModelFactory):
    class Meta:
        model = File

    original_name = factory.LazyAttribute(lambda x: x.source)
    original_hash = factory.Faker('lexify', text='fakehash:??????????????????', letters='deadbeef')
    hash = factory.Faker('lexify', text='fakehash:??????????????????', letters='deadbeef')
    size_bytes = factory.Faker('random_int')
    source = factory.Faker('file_name', extension='zip')

    user = factory.SubFactory('common.tests.factories.users.UserFactory')

    metadata = factory.Dict({})


class ImageFactory(FileFactory):
    original_name = factory.Faker('file_name', extension='png')
    source = 'images/de/deadbeef.png'
    type = File.TYPES.IMAGE
    size_bytes = 1234
