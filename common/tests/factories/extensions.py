import random

from factory.django import DjangoModelFactory
from faker import Faker
from mdgen import MarkdownPostProvider
import factory
import factory.fuzzy

from extensions.models import Extension, Version, Tag, Preview, Platform
from ratings.models import Rating

fake_markdown = Faker()
fake_markdown.add_provider(MarkdownPostProvider)


class ExtensionFactory(DjangoModelFactory):
    class Meta:
        model = Extension

    name = factory.Faker('catch_phrase')
    extension_id = factory.Faker('slug')
    slug = factory.Faker('slug')
    description = factory.LazyAttribute(
        lambda _: fake_markdown.post(size=random.choice(('medium', 'large')))
    )
    support = factory.Faker('url')
    website = factory.Faker('url')

    download_count = factory.Faker('random_int')
    view_count = factory.Faker('random_int')

    @factory.post_generation
    def previews(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for _ in extracted:
                Preview.objects.create(file=_, caption='Media Caption', extension=self)

    @factory.post_generation
    def process_extension_id(self, created, extracted, **kwargs):
        self.extension_id = self.extension_id.replace("-", "_")


class RatingFactory(DjangoModelFactory):
    class Meta:
        model = Rating

    score = factory.fuzzy.FuzzyChoice(choices=Rating.SCORES.values)
    text = factory.Faker('text')
    ip_address = factory.Faker('ipv4_private')
    status = factory.fuzzy.FuzzyChoice(choices=Rating.STATUSES.values)

    user = factory.SubFactory('common.tests.factories.users.UserFactory')


class VersionFactory(DjangoModelFactory):
    class Meta:
        model = Version

    extension = factory.SubFactory(ExtensionFactory)
    version = factory.LazyAttribute(
        lambda _: f'{random.randint(0, 9)}.{random.randint(0, 9)}.{random.randint(0, 9)}'
    )
    blender_version_min = factory.fuzzy.FuzzyChoice(
        {'2.83.1', '2.93.0', '2.93.8', '3.0.0', '3.2.1'}
    )
    download_count = factory.Faker('random_int')
    tagline = factory.Faker('bs')

    file = factory.SubFactory(
        'common.tests.factories.files.FileFactory',
        metadata=factory.Dict(
            {
                'name': factory.Faker('name'),
                'support': factory.Faker('url'),
                'website': factory.Faker('url'),
            }
        ),
    )
    ratings = factory.RelatedFactoryList(
        RatingFactory, size=lambda: random.randint(1, 50), factory_related_name='version'
    )

    @factory.post_generation
    def platforms(self, create, extracted, **kwargs):
        if not create:
            return

        if not extracted:
            return

        tags = Platform.objects.filter(slug__in=extracted)
        self.platforms.add(*tags)

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            return

        if not extracted:
            return

        tags = Tag.objects.filter(name__in=extracted)
        self.tags.add(*tags)


def create_version(**kwargs) -> 'Version':
    version = VersionFactory(**kwargs)
    file = version.file
    file.extension = version.extension
    file.save(update_fields={'extension'})
    file.extension.authors.add(version.file.user)
    return version


def create_approved_version(**kwargs) -> 'Version':
    version = create_version(**kwargs)
    version.extension.approve()
    version.refresh_from_db()
    return version
