from django.utils.translation import gettext_lazy as _
from extended_choices import Choices

# Extension author roles.
AUTHOR_ROLE_DEV = 1
AUTHOR_ROLE_CHOICES = ((AUTHOR_ROLE_DEV, _('Developer')),)

# Extension types
EXTENSION_TYPE_CHOICES = Choices(
    ('BPY', 1, _('Add-on')),
    ('THEME', 2, _('Theme')),
)
STATUS_INCOMPLETE = 1
STATUS_AWAITING_REVIEW = 2
STATUS_APPROVED = 3
STATUS_DISABLED = 4
STATUS_DISABLED_BY_AUTHOR = 5

# Extension statuses
EXTENSION_STATUS_CHOICES = Choices(
    ('INCOMPLETE', STATUS_INCOMPLETE, _('Incomplete')),
    ('AWAITING_REVIEW', STATUS_AWAITING_REVIEW, _('Awaiting Review')),
    ('APPROVED', STATUS_APPROVED, _('Approved')),
    ('DISABLED', STATUS_DISABLED, _('Disabled by staff')),
    ('DISABLED_BY_AUTHOR', STATUS_DISABLED_BY_AUTHOR, _('Disabled by author')),
)

# File types
FILE_TYPE_CHOICES = Choices(
    ('BPY', 1, _('Add-on')),
    ('THEME', 2, _('Theme')),
    # ('KEYMAP', 3, _('Keymap')),
    # ('ASSET_BUNDLE', 4, _('Asset Bundle')),
    ('IMAGE', 5, _('Image')),
    ('VIDEO', 6, _('Video')),
)

# File statuses
FILE_STATUS_CHOICES = Choices(
    ('AWAITING_REVIEW', STATUS_AWAITING_REVIEW, _('Awaiting Review')),
    ('APPROVED', STATUS_APPROVED, _('Approved')),
    ('DISABLED', STATUS_DISABLED, _('Disabled by staff')),
    ('DISABLED_BY_AUTHOR', STATUS_DISABLED_BY_AUTHOR, _('Disabled by author')),
)

# We use these slugs in browse page urls.
EXTENSION_TYPE_SLUGS = {
    EXTENSION_TYPE_CHOICES.BPY: 'add-ons',
    EXTENSION_TYPE_CHOICES.THEME: 'themes',
}
# We use these slugs in the JSON.
EXTENSION_TYPE_SLUGS_SINGULAR = {
    EXTENSION_TYPE_CHOICES.BPY: 'add-on',
    EXTENSION_TYPE_CHOICES.THEME: 'theme',
}
EXTENSION_TYPE_PLURAL = {
    EXTENSION_TYPE_CHOICES.BPY: _('Add-ons'),
    EXTENSION_TYPE_CHOICES.THEME: _('Themes'),
}
EXTENSION_SLUGS_PATH = '|'.join(EXTENSION_TYPE_SLUGS.values())
EXTENSION_SLUG_TYPES = {v: k for k, v in EXTENSION_TYPE_SLUGS_SINGULAR.items()}

ALLOWED_EXTENSION_MIMETYPES = ('application/zip',)
ALLOWED_FEATURED_IMAGE_MIMETYPES = ('image/jpg', 'image/jpeg', 'image/png', 'image/webp')
ALLOWED_ICON_MIMETYPES = ('image/png',)
# FIXME: this controls the initial widget rendered server-side, and server-side validation
# but not the additional JS-appended preview file inputs.
# If this list changes, the "accept" attribute also has to be updated in appendImageUploadForm.
ALLOWED_PREVIEW_MIMETYPES = ('image/jpg', 'image/jpeg', 'image/png', 'image/webp', 'video/mp4')

# Rating scores
RATING_SCORE_CHOICES = Choices(
    ('FIVE_STARS', 5, '★★★★★'),
    ('FOUR_STARS', 4, '★★★★'),
    ('THREE_STARS', 3, '★★★'),
    ('TWO_STARS', 2, '★★'),
    ('ONE_STAR', 1, '★'),
)

# Rating statuses
RATING_STATUS_CHOICES = Choices(
    ('AWAITING_REVIEW', STATUS_AWAITING_REVIEW, _('Awaiting Review')),
    ('APPROVED', STATUS_APPROVED, _('Approved')),
    ('REJECTED', STATUS_DISABLED, _('Rejected by staff')),
)

# Team roles.
TEAM_ROLE_MEMBER = 1
TEAM_ROLE_MANAGER = 2
TEAM_ROLE_CHOICES = (
    (TEAM_ROLE_MEMBER, _('Member')),
    (TEAM_ROLE_MANAGER, _('Manager')),
)

# Abuse
ABUSE_TYPE_EXTENSION = 1
ABUSE_TYPE_USER = 2
ABUSE_TYPE_RATING = 3

ABUSE_TYPE = Choices(
    ('ABUSE_EXTENSION', ABUSE_TYPE_EXTENSION, "Extension"),
    ('ABUSE_USER', ABUSE_TYPE_USER, "User"),
    ('ABUSE_RATING', ABUSE_TYPE_RATING, "Rating"),
)

# **N.B.**: thumbnail sizes are not intended to be changed on the fly:
# thumbnails of existing images must exist in MEDIA_ROOT before
# the code expecting thumbnails of new dimensions can be deployed!
THUMBNAIL_SIZES = {'1080p': [1920, 1080], '360p': [640, 360]}
THUMBNAIL_FORMAT = 'WEBP'
THUMBNAIL_QUALITY = 83
