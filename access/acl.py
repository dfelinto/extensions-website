def action_allowed_for(user, perm, obj=None):
    return user.has_perm(perm, obj)
