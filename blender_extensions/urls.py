"""blender_extensions URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.contrib.flatpages import views as flatpages_views
from django.urls import path, include, re_path
from django.views.static import serve
from django.views.generic.base import RedirectView

from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

import blender_id_oauth_client.urls

import common.views.errors

admin.site.enable_nav_sidebar = False
admin.site.site_header = settings.ADMIN_SITE_HEADER

urlpatterns = [
    path('oauth/', include(blender_id_oauth_client.urls)),
    path('admin/', include('loginas.urls')),
    path('admin/', admin.site.urls),
    path('', include('ratings.urls')),
    path('', include('extensions.urls')),
    path('', include('abuse.urls')),
    path('', include('users.urls')),
    path('', include('teams.urls')),
    path('', include('reviewers.urls')),
    path('', include('notifications.urls')),
    path('api/swagger/', RedirectView.as_view(url='/api/v1/swagger/')),
    path('api/v1/', SpectacularAPIView.as_view(), name='schema_v1'),
    path('api/v1/swagger/', SpectacularSwaggerView.as_view(url_name='schema_v1'), name='swagger'),
    # Flatpages
    path('about/', flatpages_views.flatpage, {'url': '/about/'}, name='flatpage-about'),
    re_path(r'^(?P<url>.*/)$', flatpages_views.flatpage),
]
handler400 = common.views.errors.ErrorView.as_view(status=400)
handler403 = common.views.errors.ErrorView.as_view(status=403)
handler404 = common.views.errors.ErrorView.as_view(status=404)
handler500 = common.views.errors.ErrorView.as_view(status=500)

if settings.DEBUG:
    urlpatterns = [
        re_path(
            r'^media/(?P<path>.*)$',
            serve,
            {
                'document_root': settings.MEDIA_ROOT,
            },
        ),
        path('__debug__/', include('debug_toolbar.urls')),
    ] + urlpatterns
