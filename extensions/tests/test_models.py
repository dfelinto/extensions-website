import json

from django.test import TestCase

from common.admin import get_admin_change_path
from common.log_entries import entries_for
from common.tests.factories.extensions import create_version
from common.tests.factories.users import UserFactory
from extensions.models import Extension


class ExtensionTest(TestCase):
    maxDiff = None
    fixtures = ['dev', 'licenses']

    def setUp(self):
        super().setUp()
        self.extension = create_version(
            file__size_bytes=123,
            extension__description='Extension description',
            extension__website='https://example.com/',
            extension__name='Extension name',
            extension__status=Extension.STATUSES.INCOMPLETE,
            extension__support='https://example.com/',
            file__metadata={
                'name': 'Extension name',
                'support': 'https://example.com/',
                'website': 'https://example.com/',
            },
        ).extension
        self.assertEqual(entries_for(self.extension).count(), 0)
        self.assertIsNone(self.extension.date_approved)
        self.assertIsNone(self.extension.date_status_changed)

    def _check_change_message(self):
        entries = entries_for(self.extension)
        self.assertEqual(entries.count(), 1)
        log_entry = entries.first()
        change_message = json.loads(log_entry.change_message)
        self.assertEqual(len(change_message), 1)
        self.assertDictEqual(
            change_message[0],
            {
                'changed': {
                    'fields': ['status'],
                    'name': 'extension',
                    'new_state': {'status': 'Approved'},
                    'object': '<Extension: Add-on "Extension name">',
                    'old_state': {
                        'description': 'Extension description',
                        'website': 'https://example.com/',
                        'name': 'Extension name',
                        'status': 1,
                        'support': 'https://example.com/',
                    },
                }
            },
        )

    def test_status_change_updates_date_creates_log_entry(self):
        self.extension.approve()

        self.assertIsNotNone(self.extension.date_approved)
        self.assertIsNotNone(self.extension.date_status_changed)
        self._check_change_message()

    def test_status_change_updates_date_creates_log_entry_with_update_fields(self):
        self.extension.approve()

        self.assertIsNotNone(self.extension.date_approved)
        self.assertIsNotNone(self.extension.date_status_changed)
        self._check_change_message()

    def test_admin_change_view(self):
        path = get_admin_change_path(obj=self.extension)
        self.assertEqual(path, '/admin/extensions/extension/1/change/')

        admin_user = UserFactory(is_staff=True, is_superuser=True)
        self.client.force_login(admin_user)
        response = self.client.get(path)

        self.assertEqual(response.status_code, 200, path)


class VersionTest(TestCase):
    maxDiff = None
    fixtures = ['dev', 'licenses']

    def setUp(self):
        super().setUp()
        self.version = create_version(
            blender_version_min='2.83.1',
            version='1.1.2',
            extension__description='Extension description',
            extension__website='https://example.com/',
            extension__name='Extension name',
            extension__status=Extension.STATUSES.INCOMPLETE,
            extension__support='https://example.com/',
        )
        self.assertEqual(entries_for(self.version).count(), 0)

    def _check_change_message(self):
        entries = entries_for(self.version)
        self.assertEqual(entries.count(), 1)
        log_entry = entries.first()
        change_message = json.loads(log_entry.change_message)
        self.assertEqual(len(change_message), 1)
        self.assertDictEqual(
            change_message[0],
            {
                'changed': {
                    'fields': ['status'],
                    'name': 'version',
                    'new_state': {'status': 'Approved'},
                    'object': '<Version: Add-on "Extension name" v1.1.2>',
                    'old_state': {
                        'blender_version_max': None,
                        'blender_version_min': '4.2.0',
                        'status': 1,
                        'version': '1.1.2',
                    },
                }
            },
        )

    def test_admin_change_view(self):
        path = get_admin_change_path(obj=self.version)
        self.assertEqual(path, '/admin/extensions/version/1/change/')

        admin_user = UserFactory(is_staff=True, is_superuser=True)
        self.client.force_login(admin_user)
        response = self.client.get(path)

        self.assertEqual(response.status_code, 200, path)


class UpdateMetadataTest(TestCase):
    fixtures = ['dev', 'licenses']

    def setUp(self):
        super().setUp()
        self.first_version = create_version(
            extension__description='Extension description',
            extension__name='name',
            extension__status=Extension.STATUSES.INCOMPLETE,
            extension__support='https://example.com/',
            extension__website='https://example.com/',
            file__metadata={
                'name': 'name',
                'support': 'https://example.com/',
                'website': 'https://example.com/',
            },
        )
        self.extension = self.first_version.extension

    def test_version_create_and_delete(self):
        second_version = create_version(
            extension=self.extension,
            file__metadata={
                'name': 'new name',
                'support': 'https://example.com/new',
                'website': 'https://example.com/new',
            },
        )
        self.extension.refresh_from_db()
        self.assertEqual(self.extension.name, 'new name')
        self.assertEqual(self.extension.support, 'https://example.com/new')
        self.assertEqual(self.extension.website, 'https://example.com/new')

        second_version.delete()
        self.extension.refresh_from_db()
        self.assertEqual(self.extension.name, 'name')
        self.assertEqual(self.extension.support, 'https://example.com/')
        self.assertEqual(self.extension.website, 'https://example.com/')

    def test_old_name_taken(self):
        second_version = create_version(
            extension=self.extension,
            file__metadata={
                'name': 'new name',
                'support': 'https://example.com/new',
                'website': 'https://example.com/new',
            },
        )

        # another extension uses old name
        create_version(
            extension__description='Extension description',
            extension__extension_id='lalalala',
            extension__name='name',
            extension__status=Extension.STATUSES.INCOMPLETE,
            extension__support='https://example.com/',
            extension__website='https://example.com/',
            file__metadata={
                'name': 'name',
                'support': 'https://example.com/',
                'website': 'https://example.com/',
            },
        )

        second_version.delete()
        self.extension.refresh_from_db()
        # couldn't revert the name because it has been taken
        self.assertEqual(self.extension.name, 'new name')
        # reverted other fields
        self.assertEqual(self.extension.support, 'https://example.com/')
        self.assertEqual(self.extension.website, 'https://example.com/')
