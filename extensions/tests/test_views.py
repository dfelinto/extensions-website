from datetime import timedelta

from django.test import TestCase
from django.urls import reverse

from common.tests.factories.extensions import create_version, create_approved_version
from common.tests.factories.users import UserFactory
from extensions.models import Extension, Version
from files.models import File
from teams.models import Team


def _create_extension():
    return create_version(
        version='1.3.4',
        blender_version_min='2.93.1',
        extension__name='Test Add-on',
        extension__description='**Description in bold**',
        extension__support='https://example.com/issues/',
        extension__website='https://example.com/',
        extension__status=Extension.STATUSES.INCOMPLETE,
        extension__average_score=2.5,
        file__metadata={
            'name': 'Test Add-on',
            'support': 'https://example.com/issues/',
            'website': 'https://example.com/',
        },
    ).extension


class _BaseTestCase(TestCase):
    fixtures = ['dev', 'licenses']

    def _check_detail_page(self, response, extension):
        pass

    def _check_ratings_page(self, response, extension):
        pass


class PublicViewsTest(_BaseTestCase):
    def test_home_page_can_view_anonymously(self):
        [create_approved_version() for _ in range(3)]

        url = reverse('extensions:home')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_search_page_can_view_anonymously(self):
        [create_approved_version() for _ in range(3)]

        url = reverse('extensions:search')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def _test_format_json(self, url, HTTP_ACCEPT=None):
        [create_approved_version() for _ in range(3)]
        response = self.client.get(url, HTTP_ACCEPT=HTTP_ACCEPT)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')
        json = response.json()
        self.assertEqual(len(json['data']), 3)
        for v in json['data']:
            self.assertIn('id', v)
            self.assertIn('name', v)
            self.assertIn('tagline', v)
            self.assertIn('version', v)
            self.assertIn('type', v)
            self.assertIn('archive_size', v)
            self.assertIn('archive_hash', v)
            self.assertIn('archive_url', v)
            self.assertIn('blender_version_min', v)
            self.assertIn('maintainer', v)
            self.assertIn('license', v)
            self.assertIn('website', v)
            self.assertIn('schema_version', v)
        return response

    def test_home_page_view_api(self):
        url = '/'
        self._test_format_json(url, HTTP_ACCEPT='application/json')

    def test_home_page_view_html(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'extensions/home.html')


class ApiViewsTest(_BaseTestCase):
    def test_blender_version_filter(self):
        create_approved_version(blender_version_min='4.0.1')
        create_approved_version(blender_version_min='4.1.1')
        create_approved_version(blender_version_min='4.2.1')
        url = reverse('extensions:api')

        json = self.client.get(
            url + '?blender_version=4.1.1',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(len(json['data']), 2)

        json2 = self.client.get(
            url + '?blender_version=3.0.1',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(len(json2['data']), 0)

        json3 = self.client.get(
            url + '?blender_version=4.3.1',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(len(json3['data']), 3)

    def test_platform_filter(self):
        create_approved_version(platforms=['windows-amd64'])
        create_approved_version(platforms=['windows-arm64'])
        create_approved_version()
        url = reverse('extensions:api')

        json = self.client.get(
            url + '?platform=windows-amd64',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(len(json['data']), 2)

    def test_blender_version_filter_latest_not_max_version(self):
        version = create_approved_version(blender_version_min='4.0.1')
        version.date_created
        extension = version.extension
        create_approved_version(
            blender_version_min='4.2.1',
            extension=extension,
            date_created=version.date_created + timedelta(days=1),
            version='2.0.0',
        )
        create_approved_version(
            blender_version_min='3.0.0',
            extension=extension,
            date_created=version.date_created + timedelta(days=2),
            version='1.0.1',
        )
        create_approved_version(
            blender_version_min='4.2.1',
            extension=extension,
            date_created=version.date_created + timedelta(days=3),
            version='2.0.1',
        )
        url = reverse('extensions:api')

        json = self.client.get(
            url + '?blender_version=4.1.1',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(len(json['data']), 1)
        # we are expecting the latest matching, not the maximum version
        self.assertEqual(json['data'][0]['version'], '1.0.1')

    def test_maintaner_is_team(self):
        version = create_approved_version(blender_version_min='4.0.1')
        team = Team(name='test team', slug='test-team')
        team.save()
        version.extension.team = team
        version.extension.save()
        url = reverse('extensions:api')

        json = self.client.get(
            url,
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(json['data'][0]['maintainer'], 'test team')


class ExtensionDetailViewTest(_BaseTestCase):
    def test_cannot_view_unlisted_extension_anonymously(self):
        extension = _create_extension()

        url = extension.get_absolute_url()
        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)

    def test_can_view_unlisted_extension_if_staff(self):
        staff_user = UserFactory(is_staff=True)
        extension = _create_extension()

        self.client.force_login(staff_user)
        response = self.client.get(extension.get_absolute_url())

        self._check_detail_page(response, extension)

    def test_can_view_unlisted_extension_if_maintaner(self):
        extension = _create_extension()

        self.client.force_login(extension.authors.first())
        response = self.client.get(extension.get_absolute_url())

        self._check_detail_page(response, extension)

    def test_can_view_publicly_listed_extension_anonymously(self):
        extension = _create_extension()
        extension.approve()

        response = self.client.get(extension.get_absolute_url())

        self._check_detail_page(response, extension)

    def test_can_view_publicly_listed_extension_versions_anonymously(self):
        extension = _create_extension()
        extension.approve()

        response = self.client.get(extension.get_versions_url())

        self._check_detail_page(response, extension)

    def test_can_view_publicly_listed_extension_ratings_anonymously(self):
        extension = _create_extension()
        extension.approve()

        response = self.client.get(extension.get_ratings_url())

        self._check_ratings_page(response, extension)


class ExtensionManageViewTest(_BaseTestCase):
    def _check_manage_page(self, response, extension):
        self.assertContains(response, 'Test Add-on')
        self.assertContains(response, '**Description in bold**', html=True)
        self.assertContains(response, 'https://example.com/issues/')
        self.assertContains(response, 'https://example.com/')
        self.assertContains(response, 'Community', html=True)

    def test_cannot_view_manage_extension_page_anonymously(self):
        extension = _create_extension()

        response = self.client.get(extension.get_manage_url())

        self.assertEqual(response.status_code, 302)

    def test_cannot_view_manage_extension_page_for_drafts(self):
        extension = _create_extension()

        response = self.client.get(extension.get_manage_url())

        self.assertEqual(response.status_code, 302)

    def test_can_view_manage_extension_page_if_maintaner(self):
        extension = _create_extension()
        extension.approve()

        self.client.force_login(extension.authors.first())
        response = self.client.get(extension.get_manage_url())

        self._check_manage_page(response, extension)


class ListedExtensionsTest(_BaseTestCase):
    def setUp(self):
        self.assertEqual(Extension.objects.count(), 0)
        self.assertEqual(Version.objects.count(), 0)
        self.version = create_approved_version()
        self.extension = self.version.extension
        self.assertEqual(Extension.objects.count(), 1)
        self.assertEqual(Version.objects.count(), 1)
        self.assertTrue(self.extension.is_listed)
        self.assertEqual(self._listed_extensions_count(), 1)

    def _listed_extensions_count(self):
        response = self.client.get('/?format=json', HTTP_ACCEPT='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

        # Basic sanity check to make sure we are getting the result of listed
        listed_count = len(response.json()['data'])
        self.assertEqual(Extension.objects.listed.count(), listed_count)
        return listed_count

    def test_list_extension_only_once(self):
        create_approved_version(extension=self.extension)
        self.assertEqual(self._listed_extensions_count(), 1)

    def test_moderate_extension(self):
        self.extension.status = Extension.STATUSES.DISABLED
        self.extension.save()
        self.assertEqual(self._listed_extensions_count(), 0)

    def test_moderate_only_version(self):
        self.version.file.status = File.STATUSES.DISABLED
        self.version.file.save()
        self.assertEqual(self._listed_extensions_count(), 0)


class UpdateVersionViewTest(_BaseTestCase):
    def test_update_view_access(self):
        extension = _create_extension()
        extension_owner = extension.latest_version.file.user
        extension.authors.add(extension_owner)

        random_user = UserFactory()

        url = reverse(
            'extensions:version-update',
            kwargs={
                'type_slug': extension.type_slug,
                'slug': extension.slug,
                'pk': extension.latest_version.pk,
            },
        )

        # Anonymous users are redirected to login
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        # Maintainers can view
        self.client.force_login(extension_owner)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # Others are forbidden
        self.client.force_login(random_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_blender_max_version(self):
        extension = _create_extension()
        extension_owner = extension.latest_version.file.user
        extension.authors.add(extension_owner)
        self.client.force_login(extension_owner)
        url = reverse(
            'extensions:version-update',
            kwargs={
                'type_slug': extension.type_slug,
                'slug': extension.slug,
                'pk': extension.latest_version.pk,
            },
        )
        version = extension.latest_version

        response = self.client.post(
            url,
            {'release_notes': 'text', 'blender_version_max': 'invalid'},
        )
        # error page, no redirect
        self.assertEqual(response.status_code, 200)
        version.refresh_from_db()
        self.assertIsNone(version.blender_version_max)

        response2 = self.client.post(
            url,
            {'release_notes': 'text', 'blender_version_max': '4.2.0'},
        )
        # success, redirect
        self.assertEqual(response2.status_code, 302)
        version.refresh_from_db()
        self.assertEqual(version.blender_version_max, '4.2.0')
