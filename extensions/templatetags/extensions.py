from django.template import Library
from django.template.defaultfilters import stringfilter

from colorhash import ColorHash

import extensions.models

register = Library()


@register.simple_tag(takes_context=True)
def has_maintainer(context, extension: extensions.models.Extension) -> bool:
    """Return True if current user is a maintainer of a given extension."""
    request = context.get('request')
    return extension.has_maintainer(request.user)


@register.simple_tag(takes_context=True)
def can_rate(context, extension: extensions.models.Extension) -> bool:
    """Return True if current user can rate a given extension."""
    request = context.get('request')
    return extension.can_rate(request.user)


@register.filter
@stringfilter
def color_hash_hex(value) -> str:
    return ColorHash(value).hex
