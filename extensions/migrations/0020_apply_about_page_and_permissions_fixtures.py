from django.db import migrations
from django.core.management import call_command


def load_fixtures(apps, schema_editor):
    call_command('loaddata', 'extensions/fixtures/version_permissions.json')
    call_command('loaddata', 'common/fixtures/flatpages.json')


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '__latest__'),
        ('extensions', '0019_remove_extension_sponsor'),
    ]

    operations = [
        migrations.RunPython(load_fixtures),
    ]
