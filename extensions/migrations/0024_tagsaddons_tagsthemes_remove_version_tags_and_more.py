# Generated by Django 4.0.6 on 2024-02-27 17:34

from django.db import migrations, models

from taggit.models import TaggedItem
import logging

from constants.base import EXTENSION_TYPE_CHOICES
from utils import slugify

logger = logging.getLogger(__name__)


themes_tags = (
    'Dark',
    'Light',
    'Print',
    'Accessibility',
    'High Contrast',
)

addons_tags = (
    '3D View',
    'Add Mesh',
    'Add Curve',
    'Animation',
    'Bake',
    'Compositing',
    'Development',
    'Game Engine',
    'Import-Export',
    'Lighting',
    'Material',
    'Modeling',
    'Mesh',
    'Node',
    'Object',
    'Paint',
    'Pipeline',
    'Physics',
    'Render',
    'Rigging',
    'Scene',
    'Sculpt',
    'Sequencer',
    'System',
    'Text Editor',
    'UV',
    'User Interface',
)


def populate_tags_database(apps, schema_editor):
    Tag = apps.get_model("extensions", "Tag")
    for tag_name in themes_tags:
        tag = Tag(
            name=tag_name,
            slug=slugify(tag_name),
            type=EXTENSION_TYPE_CHOICES.THEME,
            )
        tag.save()

    for tag_name in addons_tags:
        tag = Tag(
            name=tag_name,
            slug=slugify(tag_name),
            type=EXTENSION_TYPE_CHOICES.BPY,
            )
        tag.save()


def migrate_tags(apps, schema_editor):
    model = apps.get_model('extensions', 'version')
    Tag = apps.get_model("extensions", "Tag")
    TagsOld = apps.get_model('taggit', 'Tag')

    all_tags = TagsOld.objects.all()
    for tag_old in all_tags:
        for item in tag_old.taggit_taggeditem_items.all():
            version = model.objects.filter(id=item.object_id).first()
            # This shouldn't ever happen, but it was happening in staging.
            if version is None:
                continue
            tag = Tag.objects.filter(name=tag_old.name)
            if not tag:
                logger.warning(f'Tag not supported anymore: {tag_old.name}')
                continue
            version.tags.add(tag.first())
            version.save()


class Migration(migrations.Migration):

    dependencies = [
        ('extensions', '0023_apply_new_licenses'),
    ]

    operations = [
        migrations.RenameField(
            model_name='version',
            old_name='tags',
            new_name='tags_old',
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=128)),
                ('slug', models.SlugField(blank=False, null=False)),
                ('type', models.PositiveSmallIntegerField(choices=[(1, 'Add-on'), (2, 'Theme')], editable=False, null=False, blank=False)),
            ],
            options={
                'abstract': False,
                'unique_together': {('name', 'type')}
            },
        ),
        migrations.RunPython(populate_tags_database),
        migrations.AddField(
            model_name='version',
            name='tags',
            field=models.ManyToManyField(blank=True, related_name='versions', to='extensions.tag'),
        ),
        migrations.RunPython(migrate_tags),
        migrations.RemoveField(
            model_name='version',
            name='tags_old',
        ),
    ]
