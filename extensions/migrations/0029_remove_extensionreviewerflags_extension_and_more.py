# Generated by Django 4.2.11 on 2024-05-14 06:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('extensions', '0028_terms_flatpages_rename'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='extensionreviewerflags',
            name='extension',
        ),
        migrations.RemoveField(
            model_name='versionreviewerflags',
            name='version',
        ),
        migrations.DeleteModel(
            name='ExtensionApprovalsCounter',
        ),
        migrations.DeleteModel(
            name='ExtensionReviewerFlags',
        ),
        migrations.DeleteModel(
            name='VersionReviewerFlags',
        ),
    ]
