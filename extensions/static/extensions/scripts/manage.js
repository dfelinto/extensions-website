// TODO: improve and refactor variable namings

const formsetContainer = document.getElementById('add-img-container');
const form = document.getElementById('update-extension-form');
const btnAddImage = document.getElementById('btn-add-img');
const formsetPrefix = 'form';
const inputTotalForms = document.getElementById(`id_${formsetPrefix}-TOTAL_FORMS`);
const tagInput = document.getElementById('id_tags');

// Create function addImgUploadFormClasses
function addImgUploadFormClasses() {
  // Function intializes img upload widgets' custom classes for js manage
  const inputImgHelper = document.querySelector('.js-input-img-helper');
  const inputImgCaptionHelper = document.querySelector('.js-input-img-caption-helper');

  inputImgHelper.querySelector('input')
    .classList.add('js-input-img');

  inputImgHelper.querySelector('label')
    .classList.add('d-none');

  inputImgCaptionHelper.querySelector('input')
    .classList.add('js-input-img-caption');
}

function appendImageUploadForm() {
  const i = inputTotalForms.value;
  const formRow = document.createElement('div');
  const newFormHTML = `
  <div class="previews-list-item">
    <div class="align-items-center d-flex previews-list-item-thumbnail ps-3">
      <div class="js-input-img-thumbnail previews-list-item-thumbnail-img" title="Preview">
        <div class="align-items-center d-flex js-input-img-thumbnail-icon justify-content-center">
          <i class="i-image"></i>
        </div>
      </div>
    </div>
    <div class="details flex-grow-1">
      <div class="mb-2">
        <label for="${formsetPrefix}-${i}-caption">Caption</label>
        <input class="js-input-img-caption form-control" id="${formsetPrefix}-${i}-caption" type="text" maxlength="255" name="${formsetPrefix}-${i}-caption" placeholder="Describe the preview">
      </div>
      <div class="align-items-center d-flex justify-content-between">
        <input accept="image/jpg,image/jpeg,image/png,image/webp,video/mp4" class="form-control form-control-sm js-input-img" id="id_${formsetPrefix}-${i}-source" type="file" name="${formsetPrefix}-${i}-source">
        <ul class="pt-0">
          <li>
            <button class="btn btn-link btn-sm js-btn-reset-img-upload-form ps-2 pe-0"><i class="i-refresh"></i> Reset</button>
          </li>
          <li>
            <button class="btn btn-link btn-sm js-btn-remove-img-upload-form ps-2 pe-0"><i class="i-trash"></i> Delete</button>
          </li>
        </ul>
      </div>
    </div>
  </div>
  `;
  formRow.classList.add('ext-edit-field-row', 'fade', 'js-ext-edit-field-row');
  formRow.innerHTML = newFormHTML;
  formsetContainer.appendChild(formRow);
  inputTotalForms.value = parseInt(inputTotalForms.value) + 1;

  setTimeout(function() {
    // TODO: fix jump coming from grid gap on parent
    formRow.classList.add('show');

    // Reinit function clickThumbnail
    clickThumbnail();
  }, 20);
}

btnAddImage.addEventListener('click', function(ev) {
  ev.preventDefault();
  appendImageUploadForm();

  // Init function removeImgUploadForm
  removeImgUploadForm();

  // Init function resetImgUploadForm
  resetImgUploadForm();

  // Init function setImgUploadFormThumbnail
  setImgUploadFormThumbnail();

  return false;
});

// Create function clickThumbnail
function clickThumbnail() {
  const inputImgThumbnail = document.querySelectorAll('.js-input-img-thumbnail');

  // Create function to get parent with class
  function getClosestParent(item, className) {
    let currentElement = item.parentElement;

    while (currentElement) {
      if (currentElement.classList.contains(className)) {
        return currentElement;
      }

      currentElement = currentElement.parentElement;
    }

    return null;
  }

  inputImgThumbnail.forEach(function(item) {
    item.addEventListener('click', function() {
      const extEditFieldRow = getClosestParent(item, 'js-ext-edit-field-row');
      const inputImg = extEditFieldRow.querySelector('.js-input-img');

      // Trigger click input file
      inputImg.click();
    });
  });
}

// Create function removeImgUploadForm
function removeImgUploadForm() {
  const btnRemoveImgUploadForm = document.querySelectorAll('.js-btn-remove-img-upload-form');

  btnRemoveImgUploadForm.forEach(function(item) {
    item.addEventListener('click', function(e) {
      e.preventDefault();

      // Find the row parent
      const rowParent = this.closest('.js-ext-edit-field-row');

      // Remove the row
      rowParent.remove();
    });
  });
}

// Create function resetImgUploadForm
function resetImgUploadForm() {
  const btnResetImgUploadForm = document.querySelectorAll('.js-btn-reset-img-upload-form');

  btnResetImgUploadForm.forEach(function(item) {
    item.addEventListener('click', function(e) {
      e.preventDefault();

      // Find the row parent
      const rowParent = this.closest('.js-ext-edit-field-row');

      // Find the input image
      const inputImg = rowParent.querySelector('.js-input-img');

    // Find the input image caption
      const inputImgCaption = rowParent.querySelector('.js-input-img-caption');

      // Find the input image thumbnail
      const inputImgThumbnail = rowParent.querySelector('.js-input-img-thumbnail');

      // Find the input image thumbnail icon
      const inputImgThumbnailIcon = rowParent.querySelector('.js-input-img-thumbnail-icon');

      // Reset the selected image (if any)
      inputImg.value = ''; // Clear the input value

      // Reset the selected image caption
      inputImgCaption.value='';

      // Reset the selected image thumbnail
      inputImgThumbnail.removeAttribute('style');

      // Hide the selected image thumbnail icon
      inputImgThumbnailIcon.classList.add('d-flex');
      inputImgThumbnailIcon.classList.remove('d-none');
    });
  });
}

// Create function setImgUploadFormThumbnail
function setImgUploadFormThumbnail() {
  const inputImg = document.querySelectorAll('.js-input-img');

  inputImg.forEach(function(item) {
    item.addEventListener('change', function(e) {
      // Init file image
      const file = event.target.files[0];

      // Find the row parent
      const rowParent = this.closest('.js-ext-edit-field-row');

      // Find the thumbnail image
      const inputImgThumbnail = rowParent.querySelector('.js-input-img-thumbnail');

      // Find the input image thumbnail icon
      const inputImgThumbnailIcon = rowParent.querySelector('.js-input-img-thumbnail-icon');

      // Set thumbnail img
      if (file) {
        inputImgThumbnail.style.backgroundImage = `url('${URL.createObjectURL(file)}')`;
      }

      // Hide the selected image thumbnail icon
      inputImgThumbnailIcon.classList.add('d-none');
      inputImgThumbnailIcon.classList.remove('d-flex');
    });
  });
}

// Create function init
function init() {
  addImgUploadFormClasses();
  clickThumbnail();
  resetImgUploadForm();
  setImgUploadFormThumbnail();
}

// Configure tag selection, if present on the page
if (tagInput) {
  const taggerOptions = {
    wrap: false,
    restrict_tags: true,
    allow_spaces: true,
    link: function() {return false},
    completion: {
      list: JSON.parse(tagInput.dataset.tagList),
      delay: 1,
      min_length: 1,
    },
    tag_limit: 3,
  };
  tagger(tagInput, taggerOptions);
  // FIXME: changes to the tags input don't trigger form change
};

init();
