import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.views.generic.edit import CreateView

from extensions.models import Version, Extension
from files.forms import FileForm
from files.models import File

logger = logging.getLogger(__name__)


class UploadFileView(LoginRequiredMixin, CreateView):
    model = File
    template_name = 'extensions/submit.html'
    form_class = FileForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        drafts = Extension.objects.authored_by(user_id=self.request.user.pk).filter(
            status=Extension.STATUSES.INCOMPLETE
        )
        context['drafts'] = drafts
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_success_url(self):
        return self.extension.get_draft_url()

    @transaction.atomic
    def form_valid(self, form):
        """Create an extension and a version already, associated with the user."""
        self.file = form.instance

        parsed_extension_fields = self.file.parsed_extension_fields
        if parsed_extension_fields:
            # Try to look up extension by the same author and file info
            extension = (
                Extension.objects.authored_by(user_id=self.request.user.pk)
                .filter(type=self.file.type, **parsed_extension_fields)
                .first()
            )
            if extension:
                logger.warning(
                    'Found existing extension pk=%s for file pk=%s',
                    extension.pk,
                    self.file.pk,
                )
                return False

        # Make sure an extension has a user associated to it from the beginning, otherwise
        # it will prevent it from being re-uploaded and yet not show on My Extensions.
        self.extension = Extension.objects.update_or_create(
            type=self.file.type, **parsed_extension_fields
        )[0]
        self.extension.authors.add(self.request.user)
        self.extension.save()

        # Need to save the form to be able to use the file to create the version.
        self.object = self.file = form.save()

        self.file.extension = self.extension
        Version.objects.update_or_create(
            extension=self.extension, file=self.file, **self.file.parsed_version_fields
        )[0]

        return super().form_valid(form)
