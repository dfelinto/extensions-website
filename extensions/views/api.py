import logging

from rest_framework.response import Response
from rest_framework import serializers
from rest_framework.views import APIView
from drf_spectacular.utils import OpenApiParameter, extend_schema
from django.core.exceptions import ValidationError

from common.compare import is_in_version_range, version
from extensions.models import Extension, Platform
from extensions.utils import clean_json_dictionary_from_optional_fields


from constants.base import (
    EXTENSION_TYPE_SLUGS_SINGULAR,
)

log = logging.getLogger(__name__)


class ListedExtensionsSerializer(serializers.ModelSerializer):
    error_messages = {
        "invalid_blender_version": "Invalid blender_version: use full semantic versioning like "
        "4.2.0.",
        "invalid_platform": "Invalid platform: use notation specified in "
        "https://developer.blender.org/docs/features/extensions/schema/1.0.0/",
    }

    class Meta:
        model = Extension
        fields = ()

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.blender_version = kwargs.pop('blender_version', None)
        self.platform = kwargs.pop('platform', None)
        self._validate()
        super().__init__(*args, **kwargs)

    def _validate(self):
        if self.blender_version:
            try:
                version(self.blender_version)
            except ValidationError:
                self.fail('invalid_blender_version')
        if self.platform:
            # FIXME change to an in-memory lookup?
            try:
                Platform.objects.get(slug=self.platform)
            except Platform.DoesNotExist:
                self.fail('invalid_platform')

    def to_representation(self, instance):
        matching_version = None
        # avoid triggering additional db queries, reuse the prefetched queryset
        versions = [
            v
            for v in instance.versions.all()
            if v.file and v.file.status in instance.valid_file_statuses
        ]
        if not versions:
            return None
        versions = sorted(versions, key=lambda v: v.date_created, reverse=True)
        for v in versions:
            if self.blender_version and not is_in_version_range(
                self.blender_version,
                v.blender_version_min,
                v.blender_version_max,
            ):
                continue
            platform_slugs = set(p.slug for p in v.platforms.all())
            # empty platforms field matches any platform filter
            if self.platform and not (not platform_slugs or self.platform in platform_slugs):
                continue
            matching_version = v
            break

        if not matching_version:
            return None

        data = {
            'id': instance.extension_id,
            'schema_version': matching_version.schema_version,
            'name': instance.name,
            'version': matching_version.version,
            'tagline': matching_version.tagline,
            'archive_hash': matching_version.file.original_hash,
            'archive_size': matching_version.file.size_bytes,
            'archive_url': self.request.build_absolute_uri(matching_version.download_url),
            'type': EXTENSION_TYPE_SLUGS_SINGULAR.get(instance.type),
            'blender_version_min': matching_version.blender_version_min,
            'blender_version_max': matching_version.blender_version_max,
            'website': self.request.build_absolute_uri(instance.get_absolute_url()),
            # avoid triggering additional db queries, reuse the prefetched queryset
            'maintainer': instance.team and instance.team.name or str(instance.authors.all()[0]),
            'license': [license_iter.slug for license_iter in matching_version.licenses.all()],
            'permissions': [permission.slug for permission in matching_version.permissions.all()],
            'platforms': [platform.slug for platform in matching_version.platforms.all()],
            # TODO: handle copyright
            'tags': [str(tag) for tag in matching_version.tags.all()],
        }

        return clean_json_dictionary_from_optional_fields(data)


class ExtensionsAPIView(APIView):
    serializer_class = ListedExtensionsSerializer

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="blender_version",
                description=("Blender version to check for compatibility"),
                type=str,
            ),
            OpenApiParameter(
                name="platform",
                description=("Platform to check for compatibility"),
                type=str,
            ),
        ]
    )
    def get(self, request):
        blender_version = request.GET.get('blender_version')
        platform = request.GET.get('platform')
        qs = Extension.objects.listed.prefetch_related(
            'authors',
            'team',
            'versions',
            'versions__file',
            'versions__licenses',
            'versions__permissions',
            'versions__platforms',
            'versions__tags',
        ).all()
        serializer = self.serializer_class(
            qs,
            blender_version=blender_version,
            platform=platform,
            request=request,
            many=True,
        )
        data = [e for e in serializer.data if e is not None]
        return Response(
            {
                # TODO implement extension blocking by moderators
                'blocklist': [],
                'data': data,
                'version': 'v1',
            }
        )
