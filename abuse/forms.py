import logging

from django import forms
from django.core.exceptions import ValidationError

from common import compare
import releases.models
import abuse.models

logger = logging.getLogger(__name__)


class ReportExtensionForm(forms.ModelForm):
    class Meta:
        model = abuse.models.AbuseReport
        fields = ('reason', 'version', 'message')
        widgets = {
            'version': forms.Select,
        }

    def __init__(self, *args, **kwargs):
        """Limit 'version' choices to known releases."""
        super().__init__(*args, **kwargs)

        self.fields['version'].widget.choices = tuple(releases.models.Release.as_choices()) + (
            (None, 'Other'),
        )

    def clean_version(self):
        field = 'version'
        if not self.cleaned_data.get(field):
            # Assume that 'required' part of the validation is done already
            return
        try:
            return compare.version(self.cleaned_data[field])
        except ValidationError as e:
            self.add_error(field, forms.ValidationError([e.message], code='invalid'))


class ReportRatingForm(forms.ModelForm):
    class Meta:
        model = abuse.models.AbuseReport
        fields = ('reason', 'message')
