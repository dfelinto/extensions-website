import logging

from actstream.models import Action, Follow
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver

from constants.activity import Flag, Verb
from notifications.models import Notification

logger = logging.getLogger(__name__)

VERB2FLAGS = {
    Verb.APPROVED: [Flag.AUTHOR, Flag.REVIEWER],
    Verb.COMMENTED: [Flag.AUTHOR, Flag.REVIEWER],
    Verb.RATED_EXTENSION: [Flag.AUTHOR],
    Verb.REPORTED_EXTENSION: [Flag.MODERATOR],
    Verb.REPORTED_RATING: [Flag.MODERATOR],
    Verb.REQUESTED_CHANGES: [Flag.AUTHOR, Flag.REVIEWER],
    Verb.REQUESTED_REVIEW: [Flag.MODERATOR, Flag.REVIEWER],
    Verb.UPLOADED_NEW_VERSION: [],
}


@receiver(post_save, sender=Action)
def _create_notifications(
    sender: object,
    instance: Action,
    created: bool,
    raw: bool,
    **kwargs: object,
) -> None:
    if raw:
        return
    if not created:
        return

    if not instance.target:
        logger.warning(f'ignoring an unexpected Action without a target, verb={instance.verb}')
        return

    notifications = []

    flags = VERB2FLAGS.get(instance.verb, None)
    if flags is None:
        logger.warning(f'no follower flags for verb={instance.verb}, nobody will be notified')
        return

    followers = Follow.objects.for_object(instance.target).filter(flag__in=flags)
    user_ids = followers.values_list('user', flat=True)
    followers = get_user_model().objects.filter(id__in=user_ids)

    for recipient in followers:
        if recipient == instance.actor:
            continue
        notifications.append(Notification(recipient=recipient, action=instance))
    if len(notifications) > 0:
        Notification.objects.bulk_create(notifications)
