"""Send user notifications as emails, at most once delivery."""
import logging

from django.core.management.base import BaseCommand
from django.utils import timezone

from emails.util import construct_and_send_email
from notifications.models import Notification

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Command(BaseCommand):
    def handle(self, *args, **options):  # noqa: D102
        unprocessed_notifications = Notification.objects.filter(processed_by_mailer_at=None)
        for n in unprocessed_notifications:
            logger.info(f'processing Notification pk={n.pk}')
            n.processed_by_mailer_at = timezone.now()
            recipient = n.recipient
            if not recipient.is_subscribed_to_notification_emails:
                logger.info(f'{recipient} is not subscribed, skipping')
                n.save()
                continue
            # check that email is confirmed to avoid spamming unsuspecting email owners
            if recipient.confirmed_email_at is None:
                logger.info(f'{recipient} has unconfirmed email, skipping')
                n.save()
                continue
            n.email_sent = True
            # first mark as processed, then send: avoid spamming in case of a crash-loop
            n.save()
            logger.info(f'sending an email to {recipient}: {n.action}')
            send_notification_email(n)


def send_notification_email(notification):
    template_name = notification.template_name
    context = notification.get_template_context()
    construct_and_send_email(template_name, context, recipient_list=[notification.recipient.email])
