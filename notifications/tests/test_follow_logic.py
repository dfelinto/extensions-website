from pathlib import Path

from django.core import mail
from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from common.tests.factories.extensions import create_approved_version, create_version
from common.tests.factories.files import FileFactory
from common.tests.factories.users import UserFactory, create_moderator
from files.models import File
from notifications.models import Notification
from reviewers.models import ApprovalActivity

TEST_FILES_DIR = Path(__file__).resolve().parent / '../../extensions/tests/files'


class TestTasks(TestCase):
    fixtures = ['dev', 'licenses']

    def test_ratings(self):
        extension = create_approved_version(
            ratings=[], file__user__confirmed_email_at=timezone.now()
        ).extension
        author = extension.authors.first()
        notification_nr = Notification.objects.filter(recipient=author).count()
        some_user = UserFactory()
        self.client.force_login(some_user)
        url = extension.get_rate_url()
        response = self.client.post(url, {'score': 3, 'text': 'rating text'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(extension.ratings.count(), 1)
        new_notification_nr = Notification.objects.filter(recipient=author).count()
        self.assertEqual(new_notification_nr, notification_nr + 1)

        # Call the command that sends notification emails
        call_command('send_notification_emails')

        # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)

        email = mail.outbox[0]
        self.assertEqual(email.subject, f'Add-on rated: "{extension.name}"')
        self.assertEqual(email.to, [author.email])
        email_text = email.body
        self.maxDiff = None
        expected_text = expected_rated_text.format(**locals())
        self.assertEqual(email_text, expected_text)

        # Check that most important action and relevant URL are in the HTML version
        email_html = email.alternatives[0][0]
        self.assertIn(' rated ', email_html)
        self.assertIn(
            f'https://extensions.local:8111/add-ons/{extension.slug}/reviews/', email_html
        )

    def test_abuse(self):
        extension = create_approved_version(ratings=[]).extension
        moderator = create_moderator(confirmed_email_at=timezone.now())
        notification_nr = Notification.objects.filter(recipient=moderator).count()
        some_user = UserFactory()
        self.client.force_login(some_user)
        url = extension.get_report_url()
        response = self.client.post(
            url,
            {
                'message': 'test message',
                'reason': '127',
                'version': '',
            },
        )
        self.assertEqual(response.status_code, 302)
        report_url = response['Location']
        new_notification_nr = Notification.objects.filter(recipient=moderator).count()
        self.assertEqual(new_notification_nr, notification_nr + 1)

        call_command('send_notification_emails')

        # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)

        email = mail.outbox[0]
        self.assertEqual(email.subject, f'Add-on reported: "{extension.name}"')
        self.assertEqual(email.to, [moderator.email])
        email_text = email.body
        self.maxDiff = None
        expected_text = expected_abuse_report_text.format(**locals())
        self.assertEqual(email_text, expected_text)

        # Check that most important action and relevant URL are in the HTML version
        email_html = email.alternatives[0][0]
        self.assertIn(report_url, email_html)
        self.assertIn(' reported ', email_html)

    def test_new_extension_submitted(self):
        moderator = create_moderator(confirmed_email_at=timezone.now())
        notification_nr = Notification.objects.filter(recipient=moderator).count()
        some_user = UserFactory()
        file_data = {
            'metadata': {
                'tagline': 'Get insight on the complexity of an edit',
                'id': 'edit_breakdown',
                'name': 'Edit Breakdown',
                'version': '0.1.0',
                'blender_version_min': '4.2.0',
                'type': 'add-on',
                'schema_version': "1.0.0",
            },
            'file_hash': 'sha256:4f3664940fc41641c7136a909270a024bbcfb2f8523a06a0d22f85c459b0b1ae',
            'size_bytes': 53959,
            'tags': ['Sequencer'],
            'version_str': '0.1.0',
            'slug': 'edit-breakdown',
        }
        file = FileFactory(
            type=File.TYPES.BPY,
            user=some_user,
            original_hash=file_data['file_hash'],
            hash=file_data['file_hash'],
            metadata=file_data['metadata'],
        )
        create_version(
            file=file,
            extension__name=file_data['metadata']['name'],
            extension__slug=file_data['metadata']['id'].replace("_", "-"),
            extension__website=None,
            tagline=file_data['metadata']['tagline'],
            version=file_data['metadata']['version'],
            blender_version_min=file_data['metadata']['blender_version_min'],
            schema_version=file_data['metadata']['schema_version'],
        )
        self.client.force_login(some_user)
        data = {
            # Most of these values should come from the form's initial values, set in the template
            # Version fields
            'release_notes': 'initial release',
            # Extension fields
            'description': 'Rather long and verbose description',
            'support': 'https://example.com/issues',
            # Previews
            'form-TOTAL_FORMS': ['2'],
            'form-INITIAL_FORMS': ['0'],
            'form-MIN_NUM_FORMS': ['0'],
            'form-MAX_NUM_FORMS': ['1000'],
            'form-0-id': '',
            'form-0-caption': ['First Preview Caption Text'],
            'form-1-id': '',
            'form-1-caption': ['Second Preview Caption Text'],
            # Submit for Approval.
            'submit_draft': '',
        }
        file_name1 = 'test_preview_image_0001.png'
        file_name2 = 'test_preview_image_0002.png'
        with open(TEST_FILES_DIR / file_name1, 'rb') as fp1, open(
            TEST_FILES_DIR / file_name2, 'rb'
        ) as fp2:
            files = {
                'form-0-source': fp1,
                'form-1-source': fp2,
            }
            self.client.post(file.get_submit_url(), {**data, **files})
        new_notification_nr = Notification.objects.filter(recipient=moderator).count()
        self.assertEqual(new_notification_nr, notification_nr + 1)

        call_command('send_notification_emails')

        # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)

        email = mail.outbox[0]
        extension = file.version.extension
        self.assertEqual(email.subject, 'Add-on review requested: "Edit Breakdown"')
        self.assertEqual(email.to, [moderator.email])
        email_text = email.body
        self.maxDiff = None
        expected_text = expected_review_requested_text.format(**locals())
        self.assertEqual(email_text, expected_text)

        # Check that most important action and relevant URL are in the HTML version
        email_html = email.alternatives[0][0]
        self.assertIn(' requested review of ', email_html)
        self.assertIn(f'https://extensions.local:8111/approval-queue/{extension.slug}/', email_html)

    def test_approval_queue_activity(self):
        extension = create_approved_version(ratings=[]).extension
        author = extension.authors.first()
        moderator = create_moderator(confirmed_email_at=timezone.now())
        some_user = UserFactory()
        notification_nrs = {}
        for user in [author, moderator, some_user]:
            notification_nrs[user.pk] = Notification.objects.filter(recipient=user).count()
        # both moderator and some_user start following only after their first comment
        self._leave_a_comment(moderator, extension, 'need to check this')
        self._leave_a_comment(some_user, extension, 'this is bad')
        self._leave_a_comment(moderator, extension, 'thanks for the heads up')
        new_notification_nrs = {}
        for user in [author, moderator, some_user]:
            new_notification_nrs[user.pk] = Notification.objects.filter(recipient=user).count()
        self.assertEqual(new_notification_nrs[author.pk], notification_nrs[author.pk] + 3)
        self.assertEqual(new_notification_nrs[moderator.pk], notification_nrs[moderator.pk] + 1)
        self.assertEqual(new_notification_nrs[some_user.pk], notification_nrs[some_user.pk] + 1)

        call_command('send_notification_emails')

        # Test that one message has been sent (only moderator has email confirmed here).
        self.assertEqual(len(mail.outbox), 1)

        email = mail.outbox[0]
        self.assertEqual(email.subject, f'New comment on Add-on "{extension.name}"')
        email_text = email.body
        expected_text = expected_new_comment_text.format(**locals())
        self.maxDiff = None
        self.assertEqual(email_text, expected_text)

        # Check that most important action and relevant URL are in the HTML version
        email_html = email.alternatives[0][0]
        self.assertIn(' commented on ', email_html)
        self.assertIn(f'https://extensions.local:8111/approval-queue/{extension.slug}/', email_html)

    def _leave_a_comment(self, user, extension, text):
        self.client.force_login(user)
        url = reverse('reviewers:approval-comment', args=[extension.slug])
        self.client.post(url, {'type': ApprovalActivity.ActivityType.COMMENT, 'message': text})

    # TODO: test for notifications about a reported rating
    # TODO: test for notifications about extension approved by moderators


expected_abuse_report_text = """Add-on reported: "{extension.name}"
{some_user.full_name} reported Add-on "{extension.name}"
:

“test message”

https://extensions.local:8111{report_url}
Read all notifications at https://extensions.local:8111/notifications/


Unsubscribe by adjusting your preferences at  https://extensions.local:8111/settings/profile/

https://extensions.local:8111/
"""

expected_new_comment_text = """New comment on Add-on "{extension.name}"
{some_user.full_name} commented on Add-on "{extension.name}"
:

“this is bad”

https://extensions.local:8111/approval-queue/{extension.slug}/
Read all notifications at https://extensions.local:8111/notifications/


Unsubscribe by adjusting your preferences at  https://extensions.local:8111/settings/profile/

https://extensions.local:8111/
"""

expected_rated_text = """Add-on rated: "{extension.name}"
{some_user.full_name} rated extension Add-on "{extension.name}"
:

“rating text”

https://extensions.local:8111/add-ons/{extension.slug}/reviews/
Read all notifications at https://extensions.local:8111/notifications/


Unsubscribe by adjusting your preferences at  https://extensions.local:8111/settings/profile/

https://extensions.local:8111/
"""

expected_review_requested_text = """Add-on review requested: "Edit Breakdown"
{some_user.full_name} requested review of Add-on "Edit Breakdown"
:

“Extension is ready for initial review”

https://extensions.local:8111/approval-queue/edit-breakdown/
Read all notifications at https://extensions.local:8111/notifications/


Unsubscribe by adjusting your preferences at  https://extensions.local:8111/settings/profile/

https://extensions.local:8111/
"""
